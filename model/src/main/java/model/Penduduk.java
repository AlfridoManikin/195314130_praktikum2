/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public abstract class Penduduk {
    private String nama;
    private String tempatTanggalLahir;
    //private ArrayList<Penduduk> anggota;

    public Penduduk() {
    }

    public Penduduk(String dataNama, String dataTempatTanggalLahir) {
        this.nama = dataNama;
        this.tempatTanggalLahir = dataTempatTanggalLahir;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String dataNama) {
        this.nama = dataNama;
    }

    public String getTempatTanggalLahir() {
        return tempatTanggalLahir;
    }

    public void setTempatTanggalLahir(String dataTempatTanggalLahir) {
        this.tempatTanggalLahir = dataTempatTanggalLahir;
    }
    
    public abstract double hitungIuran();
    
//    public void tambahMahasiswa(Mahasiswa mhs){
//        anggota.add(mhs);
//    }
//    
//    public void tambahMasyarakat(MasyarakatSekitar umum){
//        anggota.add(umum);
//    }
//    
//    public void tambahUKM(UKM ukm){
//        anggota.add(ukm);
//    }
    
}

